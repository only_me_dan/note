package com.note.api.Seeder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import com.note.api.model.Note;
import com.note.api.model.Notebook;
import com.note.api.services.NoteService;
import com.note.api.services.NotebookService;
 
/**
 * This component will only execute (and get instantiated) if the
 * property noteit.db.recreate is set to true in the
 * application.properties file
 */

@Component
@ConditionalOnProperty(name = "note.api.db.recreate", havingValue = "true")
public class DbSeeder implements CommandLineRunner {
	
	@Autowired
    private NotebookService notebookService;
	
	@Autowired
    private NoteService noteService;

    public DbSeeder(NotebookService notebookService,
                   NoteService noteService) {
        this.notebookService = notebookService;
        this.noteService = noteService;
    }


    @Override
    public void run(String... args) {
        // Remove all existing entities
//        this.notebookService.deleteAll();
//        this.noteService.deleteAll();


        // Save a default notebook
//        Notebook defaultNotebook = new Notebook("Default");
//        this.notebookService.save(defaultNotebook);
//
//        Notebook quotesNotebook = new Notebook("Quotes");
//        this.notebookService.save(quotesNotebook);
//
//        // Save the welcome note
//        Note note = new Note("Hello", "Welcome to Note It", defaultNotebook);
//        this.noteService.save(note);
//
//        // Save a quote note
//        Note quoteNote = new Note("Latin Quote", "Carpe Diem", quotesNotebook);
//        this.noteService.save(quoteNote);

        System.out.println("Initialized database");
    }
}