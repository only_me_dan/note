package com.note.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "Notebook")
@Table(name = "Notebook")
public class Notebook implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;
	
	@Column(name = "name", nullable = false)
	private String name;

	@OneToMany(mappedBy = "notebook")
	@JsonIgnore
	private List<Note> notes;

	@Version
	@Column(name = "version_id")
	private int versionId;
	
	public Notebook() {
		this.id = new Long(id);
		this.notes = new ArrayList();
	}

	public Notebook(String name) {
		this();
		this.name = name;
	}

	protected Notebook(String id, String name) {
		this();
		if (id != null) {
			this.id = new Long(id);
		}
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Note> getNotes() {
		return notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public int getVersionId() {
		return versionId;
	}

	public void setVersionId(int versionId) {
		this.versionId = versionId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Notebook [id=" + id + ", name=" + name + ", notes=" + notes + ", versionId=" + versionId + "]";
	}

}
