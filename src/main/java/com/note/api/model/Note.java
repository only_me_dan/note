package com.note.api.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity(name = "Note")
@Table(name = "Note")
@EntityListeners(AuditingEntityListener.class)
public class Note implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "text", nullable = false)
	private String text;

	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "notebook", nullable = true)
	private Notebook notebook;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lastModifiedOn", nullable = false)
	@LastModifiedDate
	private Date lastModifiedOn;

	@Version
	@Column(name = "version_id")
	private int versionId;

	protected Note() {
		this.id = id;
		this.lastModifiedOn = lastModifiedOn;
	}

	public Note(String title, String text, Notebook notebook) {
		this();
		this.title = title;
		this.text = text;
		this.notebook = notebook;
	}

	public Note(String id, String title, String text, Notebook notebook) {
		this(title, text, notebook);
		if (id != null) {
			this.id = new Long(id);
		}
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Notebook getNotebook() {
		return notebook;
	}

	public void setNotebook(Notebook notebook) {
		this.notebook = notebook;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public int getVersionId() {
		return versionId;
	}

	public void setVersionId(int versionId) {
		this.versionId = versionId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Note [id=" + id + ", title=" + title + ", text=" + text + ", notebook=" + notebook + ", lastModifiedOn="
				+ lastModifiedOn + ", versionId=" + versionId + "]";
	}

}
