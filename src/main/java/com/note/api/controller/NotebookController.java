package com.note.api.controller;

import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.note.api.model.Notebook;
import com.note.api.services.NotebookService; 

@RestController
@RequestMapping("/api/notebooks")
@CrossOrigin
@ComponentScan("com.notepad.api.service")
public class NotebookController {

	@Autowired
	private NotebookService notebookService;


	@GetMapping("/all")
	public List<Notebook> all() {
		return notebookService.findAll();
	}
	
	@PostMapping
    public Notebook save(@RequestBody Notebook notebookCreate,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidationException();
        }
       Notebook savedNotebook= notebookService.save(notebookCreate);
        
        return savedNotebook;
	}
	
	@DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
		notebookService.deleteById(id);
	}

}
