package com.note.api.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.note.api.dao.NoteRepository;
import com.note.api.exception.ResourceNotFoundException;
import com.note.api.model.Note;
import com.note.api.services.NoteService;

@RestController
@RequestMapping("/api/notes")
@CrossOrigin
//@ComponentScan("com.notepad.api.service")
public class NoteController {
	@Autowired
	NoteService noteService;

	@GetMapping("/all")
	public List<Note> all() {
		return (List<Note>) noteService.findAll();
	}

	@GetMapping("/byId/{id}")
	public Note getNoteById(@PathVariable(value = "id") Long noteId) {
		return noteService.findById(noteId).orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
	}

	@GetMapping("/byNotebook/{notebookId}")
	public List<Note> byNotebook(@PathVariable String notebookId) {
		return noteService.findAllByNotebookId(notebookId);
	}

	@PostMapping
	public Note save(@RequestBody Note noteCreate, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		System.out.println(noteCreate.toString());
		Note savedNote = noteService.save(noteCreate);
		return savedNote;
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long noteId) {
		Note note = noteService.findById(noteId)
				.orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));

		noteService.delete(note);

		return ResponseEntity.ok().build();
	}

	@PutMapping("/note/{id}")
	public Note updateNote(@PathVariable(value = "id") Long noteId, @Valid @RequestBody Note noteDetails) {

		Note note = noteService.findById(noteId)
				.orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
		System.out.println("======================================>  "+noteDetails.toString());
System.out.println("======================================>  "+note.toString());
		note.setTitle(noteDetails.getTitle());
		note.setText(noteDetails.getText());
		System.out.println("======================================>  "+note.toString());
		Note updatedNote = noteService.save(note);
		return updatedNote;
	}
}
