package com.note.api.services;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import com.note.api.dao.NotebookRepository;
import com.note.api.model.Notebook; 

@Service
@Transactional 
public class NotebookServiceImpl implements NotebookService {

	@Autowired
	NotebookRepository notebookRepository;
	
	
	@Override
	public List<Notebook> findAll() { 
		return (List<Notebook>) notebookRepository.findAll();
	}

	@Override
	public Notebook save(Notebook notebookCreate) { 
		return notebookRepository.save(notebookCreate);
	}

	@Override
	public void deleteById(String id) {
		notebookRepository.deleteById(new Long(id));
	}

	@Override
	public Optional<Notebook> findById(String id) {
		return notebookRepository.findById(new Long(id));
	}

	@Override
	public void deleteAll() {
		notebookRepository.deleteAll();
		
	}

}
