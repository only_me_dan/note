package com.note.api.services;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import com.note.api.dao.NoteRepository;
import com.note.api.model.Note; 

@Service
@Transactional
public class NoteServiceImpl implements NoteService {

	@Autowired
	NoteRepository noteRepository;
	
	@Autowired
	NotebookService  notebookService;
	
	@Override
	public List<Note> findAll() { 
		return  (List<Note>) noteRepository.findAll();
	}
	@Override
	public Optional<Note> findById(long id) { 
		return noteRepository.findById(id);
	}
	@Override
	public List<Note> findAllByNotebookId(String notebookId) { 
		return noteRepository.findAllByNotebook(notebookService.findById(notebookId));
	}
	@Override
	public Note save(Note noteCreate) { 
		return noteRepository.save(noteCreate);
	}
	@Override
	public void deleteById(String id) {
		noteRepository.deleteById(new Long(id));
		
	}
	@Override
	public void delete(Note note) {
		noteRepository.delete(note);
		
	}
	@Override
	public void deleteAll() {
		noteRepository.deleteAll();
		
	}

}
