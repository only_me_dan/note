package com.note.api.services;

import java.util.List;
import java.util.Optional;

import com.note.api.model.Note;

public interface NoteService {

	List<Note> findAll();

	Optional<Note> findById(long id);

	List<Note> findAllByNotebookId(String notebookId);

	Note save(Note noteCreate);

	void deleteById(String id);

	void delete(Note note);

	void deleteAll();

}
