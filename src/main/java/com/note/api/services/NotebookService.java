package com.note.api.services;

import java.util.List;
import java.util.Optional;

import com.note.api.model.Notebook;

public interface NotebookService {

	List<Notebook> findAll();

	 Optional<Notebook> findById(String id);

	Notebook save(Notebook notebookCreate);

	void deleteById(String id);

	void deleteAll();

}
