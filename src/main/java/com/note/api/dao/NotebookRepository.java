package com.note.api.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.note.api.model.Notebook;

@Repository
public interface NotebookRepository extends CrudRepository<Notebook, Long> {

}
