package com.note.api.dao;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.note.api.model.Note;
import com.note.api.model.Notebook;

@Repository
public interface NoteRepository extends CrudRepository<Note, Long> {

	List<Note> findAllByNotebook(Optional<Notebook> findById);
	
}
